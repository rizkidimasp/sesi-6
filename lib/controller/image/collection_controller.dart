import 'package:flutter/material.dart';

import '../../data/const.dart';
import '../../data/image/image.dart';

class FeedController extends ChangeNotifier {
  final List<MyImage> _images = kImageList;
  final List<MyImage> _collection = [];
  final List<MyImage> _like = [];

  List<MyImage> get myCollection => _collection;
  List<MyImage> get myImages => _images;
  List<MyImage> get myLike => _like;

  void mark(MyImage image) {
    _collection.add(image);
    notifyListeners();
  }

  void unmark(MyImage image) {
    _collection.remove(image);
    notifyListeners();
  }

  bool isMarked(MyImage image) {
    return _collection.contains(image);
  }

  void liked(MyImage image) {
    _like.add(image);
    notifyListeners();
  }

  void unliked(MyImage image) {
    _like.remove(image);
    notifyListeners();
  }

  bool isLiked(MyImage image) {
    return _like.contains(image);
  }

  void removeAll() {
    _collection.clear();
    notifyListeners();
  }
}
